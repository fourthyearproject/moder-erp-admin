import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import { store } from './store/store'
import VueToastr from '@deveodk/vue-toastr'
import VeeValidate from 'vee-validate';
import router from './router/index'
import App from './App.vue'


//Load assets
// css
import './assets/css/bootstrap-clearmin.min.css';
import  './assets/css/roboto.css';
import './assets/css/material-design.css';
import './assets/css/small-n-flat.css';
import './assets/css/font-awesome.min.css';
import '@deveodk/vue-toastr/dist/@deveodk/vue-toastr.css'

// JS
import './assets/js/lib/jquery-2.1.3.min';
import './assets/js/jquery.mousewheel.min';
import './assets/js/jquery.cookie.min';
import './assets/js/fastclick.min';
import './assets/js/bootstrap.min';
import './assets/js/clearmin.min';
import './assets/js/demo/home';

Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(VeeValidate);
Vue.use(VueToastr, {
    defaultPosition: 'toast-top-right',
    defaultType: 'info',
    defaultTimeout: 5000
});


router.beforeEach((to, from, next) => {
    // if (to.meta.requiresAuth && !account.methods.hasValidToken()) {
    //     next({name: 'Login'});
    // }
    next();
});

new Vue({
    router,
    store: store,
  render: h => h(App)
}).$mount('#app');
