import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export const store = new Vuex.Store({
    strict: true,

    state: {
        categories: [],
        models:[],
        brands: [],
        certificates: [],
        user: JSON.parse(localStorage.getItem('user'))
    },

    getters: {

    },

    mutations: {
        setCategories: (state, categories) => {
            state.categories = categories
        },
        setModels: (state, models) => {
            state.models = models
        },
        setCertificates: (state, certificates) => {
            state.certificates = certificates
        },
        setBrands: (state, brands) => {
            state.brands = brands
        }
    },

    actions: {

    }
});
