import key from './api_key';

const globals = {
    BASE_URL: 'http://construction-trader.test/api/',
    API_KEY: key.API_KEY
};

export default globals;