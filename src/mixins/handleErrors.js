const UNAUTHORIZED_STATUS = 401;
const VALIDATION_ERROR_STATUS = 403;
export default {
    methods: {
        displayError: function (error){
            if(error.status === VALIDATION_ERROR_STATUS){
                if (error.data.errors) {
                    for (let key in error.data.errors) {
                        if (error.data.errors.hasOwnProperty(key)) {
                            this.$toastr('error', error.data.errors[key]);
                        }
                    }
                } else {
                    this.$toastr('error', 'Email and password provided did not match our records, Please confirm your credential and try to login again', 'Invalid Credentials');
                }
            } else if (error.status === UNAUTHORIZED_STATUS) {
                this.$toastr('error', 'You not authorized to invoke the operation.', '403 Forbidden Error');
            } else {
                this.$toastr('error', 'The server encountered an internal error or misconfiguration and was unable to complete your request', '500 Internal Server Error');
            }
        }
    }
}