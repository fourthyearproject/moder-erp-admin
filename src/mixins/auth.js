const UNAUTHORIZED_STATUS = 401;
import global from '../environment/env';
export default {
    methods: {
        basicAuth: function (username, password){
            return 'Basic ' + btoa(username +':'+password);
        },

        login: function (data) {
            this.$http.post(global.BASE_URL + 'login','',{headers: {'Authorization': this.basicAuth(data.username, data.password), 'X-api-key': global.API_KEY}}).then(
                data => {
                    this.$toastr('success', 'Logged in successfully');
                    this.signInUser(data.body);
                },
                error => {
                    console.log(error);
                    if(error.status === UNAUTHORIZED_STATUS){
                        if (error.data.errors) {
                            for (let key in error.data.errors) {
                                if (error.data.errors.hasOwnProperty(key)) {
                                    this.$toastr('error', error.data.errors[key]);
                                }
                            }
                        } else {
                            this.$toastr('error', 'Email and password provided did not match our records, Please confirm your credential and try to login again', 'Invalid Credentials');
                        }
                    } else if (error.status === 403) {
                        this.$toastr('error', 'You not authorized to invoke the operation.', '403 Forbidden Error');

                    } else {
                        this.$toastr('error', 'The server encountered an internal error or misconfiguration and was unable to complete your request', '500 Internal Server Error');
                    }
                    console.log(error.data);
                }
            )

        },

        signInUser: function (userData) {
            let expiresAt = Date.now() + (userData.expiresIn * 1000);
            localStorage.setItem('token', userData.token);
            localStorage.setItem('user', JSON.stringify(userData.user));
            localStorage.setItem('expiresAt', expiresAt.toString());

            let redirectUrl = localStorage.getItem('redirectTo');
            if(redirectUrl){
                this.$router.push(redirectUrl);
            } else {
                this.$router.push('/');
            }
        },

        register: function (data) {
            this.$http.post(global.BASE_URL + 'register',
                JSON.stringify(data),
                {
                    headers: {
                        'X-api-key': global.API_KEY
                    }
                }).then(
                data => {
                    this.$toastr('success', 'Successfully registered');
                    this.signInUser(data.body);
                },
                error => {
                    if(error.status === UNAUTHORIZED_STATUS){
                        if (error.data.errors) {
                            for (let key in error.data.errors) {
                                if (error.data.errors.hasOwnProperty(key)) {
                                    this.$toastr('error', error.data.errors[key]);
                                }
                            }
                        } else {
                            this.$toastr('error', 'The server encountered an internal error or misconfiguration and was unable to complete your request');
                        }
                    } else if (error.status === 403) {
                        this.$toastr('error', 'You not authorized to invoke the operation.', '403 Forbidden Error');

                    } else {
                        this.$toastr('error', 'The server encountered an internal error or misconfiguration and was unable to complete your request', '500 Internal Server Error');
                    }
                    console.log(error.data);
                }
            );

        }
    }
}