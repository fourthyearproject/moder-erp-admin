import env from '../environment/env';

export const METHOD_POST = "POST";
export const METHOD_GET = "GET";
export const METHOD_PUT = "PUT";

export const STATUS_NOT_FOUND = 404;

export default {
    methods: {
        doRequest: function (endpoint, method, data) {

            return new Promise((resolve, reject) => {

                let request = {};
                request.method = method;
                request.headers = {};
                request.headers['X-api-key'] = env.API_KEY;
                request.headers['Authorization'] = 'Bearer ' + localStorage.getItem('token');

                let onSuccess = (resp) => {
                    resolve(resp.body);
                };

                let onFail = (errorData) => {
                    reject(errorData);
                };

                const url = env.BASE_URL + endpoint;
                switch (request.method) {
                    case METHOD_GET:
                        this.$http.get(url, request).then(onSuccess, onFail);
                        break;
                    case METHOD_PUT:
                        this.$http.put(url, JSON.stringify(data), request).then(onSuccess, onFail);
                        break;
                    default:
                        const dataStr = JSON.stringify(data);
                        this.$http.post(url, dataStr, request).then(onSuccess, onFail);
                }

            });
        },

        post: function (endpoint, data) {
            return this.doRequest(endpoint, METHOD_POST, data);
        },

        put: function (endpoint, data) {
            return this.doRequest(endpoint, METHOD_PUT, data);
        },

        get: function (endpoint) {
            return this.doRequest(endpoint, METHOD_GET);
        },

        getIterables: function (endpoint, offset, count, request) {
            return new Promise((resolve, reject) => {

                if (!request) request = {};
                if (!request.params) request.params = {};
                request.params.offset = offset;
                request.params.count = count;

                this.get(endpoint, request).then(
                    iterables => resolve(iterables),
                    errorData => {
                        if (errorData.status === STATUS_NOT_FOUND) {
                            resolve([]);
                            return;
                        }
                        reject(errorData);
                    }
                )
            })
        }
    }
};
