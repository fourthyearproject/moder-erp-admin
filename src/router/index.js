import Vue from 'vue'
import Router from 'vue-router'
import Login from '../components/auth/Login'
import Register from '../components/auth/Register'
import PassEmail from '../components/auth/RequestPassReset'
import PassReset from '../components/auth/ResetPassword'
import Home from '../components/HomeComponent'
import PageNotFound from '../components/404'

Vue.use(Router);

export default new Router({

    mode: 'history',

    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home,
            meta: {requiresAuth: true}
        },

        // Auth Routes
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/logout',
            name: 'Logout',
            redirect: 'Login'
        },
        {
            path: '/register',
            name: 'Register',
            component: Register
        },
        {
            path: '/password/email',
            name: 'PassEmail',
            component: PassEmail
        },
        {
            path: '/password/reset',
            name: 'PassReset',
            component: PassReset
        },

        {
            path: '*',
            name: 'PageNotFound',
            component: PageNotFound
        }
    ]

})